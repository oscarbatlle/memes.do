<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Memes.do - Login</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading">Por favor introduce tus credenciales</div>

                    <div class="panel-body">
                        <div class="col-md-6 socialite omb_login">
                            <div class="omb_row-sm-offset-3 omb_socialButtons">
                                    <a href="/login/facebook" class="btn btn-lg btn-block omb_btn-facebook">
                                        <i class="fa fa-facebook visible-xs"></i>
                                        <span class="hidden-xs">Facebook</span>
                                    </a>
                                    <a href="#" class="btn btn-lg btn-block omb_btn-twitter">
                                        <i class="fa fa-twitter visible-xs"></i>
                                        <span class="hidden-xs">Twitter</span>
                                    </a>
                                    <a href="#" class="btn btn-lg btn-block omb_btn-google">
                                        <i class="fa fa-google-plus visible-xs"></i>
                                        <span class="hidden-xs">Google+</span>
                                    </a>
                                <a href="/login/github" class="btn btn-lg btn-block omb_btn-github">
                                    <i class="fa fa-github visible-xs"></i>
                                    <span class="hidden-xs">Github</span>
                                </a>
                            </div>
                        </div>
                        <!-- end of div -->
                        <div class="col-md-6 login">
                            @include('alerts.alert')
                            <form method="POST" action="/auth/login">
                                {!! csrf_field() !!}

                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                </div>

                                <div class="form-group">
                                    <label for="password">Password:</label>
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>

                                <div class="form-group">
                                    <input type="checkbox" name="remember"> Remember Me
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary form-control">Login</button>
                                </div>
                                <a href="/password/email">Forgot password ?</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>