<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>Memes.do | Memes Dominicanos</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
{{--navbar section--}}
@include('partials.nav')

{{--content section--}}
<div class="container">

    <div class="row">

        <main class="col-md-9">
            @yield('content')
        </main>
        {{--end of main--}}

        <aside class="col-md-3">
            @include('partials.sidebar')
        </aside>
    </div>
    {{--end of div--}}
</div>
{{--end of div container--}}

{{--display login form--}}
@include('auth.login')

{{--display register form--}}
@include('auth.register')

{{--footer section--}}
@include('partials.footer')

{{--Add scripts to footer--}}
@yield('footer')

</body>
</html>
