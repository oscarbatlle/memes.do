<nav class="navbar navbar-fixed-top navbar-custom">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">
                <span class="glyphicon glyphicon-home"></span> GMO
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
            <ul class="nav navbar-nav">
                <li><a ng-href="#"><i class="fa fa-random"></i> Aleatorio</a></li>
                <li><a ng-href="#"><i class="fa fa-bolt"></i> Populares</a></li>
                <li>
                    <form class="navbar-form" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control searchbar" placeholder="Buscar" name="q">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right acme-navbar-text">
                <li><a ng-href="/create"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Crear</a></li>
                <li><a ng-href="#"><span class="glyphicon glyphicon-bell" aria-hidden="true"></span>
                        Notificaciones</a></li>
                <li><a href="#" ng-click="vm.loginModal()"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Login</a></li>
            </ul>
        </div>
    </div>
</nav>
