@extends('app')

@section('content')
    <h1>{{ $meme->title }}</h1>
    <img class="img-responsive" src="{{ URL::to('/') . '/memes/created/' . basename($meme->meme_url) . '.jpg' }}"
         alt="{{$meme->slug}}">
@stop