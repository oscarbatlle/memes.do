@extends('app')

@section('content')

    @foreach($memes as $meme)
        <div class="row post">
            <div class="col-md-8">
                <div class="visible-xs">
                    <h4>{{ $meme->title }}</h4>

                    <p>Por <strong>{{ $meme->author }}</strong></p>
                </div>
                <a href="{{ $meme->meme_url }}"><img class="img-responsive post-image"
                                                     src="{{ 'memes/created/' . basename($meme->meme_url) . ".jpg" }}"
                                                     alt="{{$meme->title}}"></a>
            </div>
            <!-- end of div -->
            <div class="col-md-4 post-sidebar socialbox">
                <div class="visible-lg">
                    <h4>{{ $meme->title }}</h4>

                    <p>Por <strong>{{ $meme->author }}</strong></p>
                </div>

                <div class="votes" data-entry-id="{{ $meme->meme_id }}" >
                    <table class="table table-responsive visible-md visible-lg">
                          <tr>
                            <td class="like">
                                <a href="#"><i class="fa fa-smile-o"></i>
                                    <div class="like-count" id="likes">
                                        {{$meme->getLikes($meme->meme_id)}}
                                    </div>
                                </a>
                             </td>
                             <td>
                                <a href="#"><i class="fa fa-comments-o"></i>
                                    <div class="comments-count" id="comments">
                                        0
                                    </div>
                                </a>
                            </td>
                          </tr>
                          <tr>
                              <td><a href="#"><i class="fa fa-facebook-square"></i></a></td>
                              <td><a href="#"><i class="fa fa-twitter-square"></i></a></td>
                          </tr>
                      </table>

                    <div class="visible-xs mobile text-center">
                        <div class="row">
                            <div class="col-xs-4">
                                <a href="#"><i class="fa fa-facebook-square"></i> Compartir</a>
                            </div>
                            <div class="col-xs-4">
                                <a href="#"><i class="fa fa-twitter-square"></i> Twittear</a>
                            </div>
                            <div class="col-xs-4">
                                <a href="#"><i class="fa fa-whatsapp"></i> Whatsapp</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="#"><i class="fa fa-comments-o"></i> Comentarios</a>
                            </div>
                            <div class="col-xs-6">
                                <a href="#"><i class="fa fa-smile-o"></i> Me gusta</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of div votes-->
            </div>
            {{--end of column--}}
        </div>
        <!-- end of div -->
    @endforeach
    {!! $memes->render() !!}
@stop
