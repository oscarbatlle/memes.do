@extends('app')

@section('content')
    <div class="well">
        <div id="myCarousel" class="carousel slide" data-interval="false">

            <!-- Carousel items -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                        <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="memes/bird1.jpg" alt="Image"
                                                                                  class="img-responsive"></a>
                        </div>
                        <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="memes/bird2.jpg" alt="Image"
                                                                                  class="img-responsive"></a>
                        </div>
                        <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="memes/bird3.jpg" alt="Image"
                                                                                  class="img-responsive"></a>
                        </div>
                        <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="memes/bird4.jpg" alt="Image"
                                                                                  class="img-responsive"></a>
                        </div>
                    </div>
                    <!--/row-->
                </div>
                <!--/item-->
                @foreach(array_chunk($images, 4) as $data)
                    <div class="item">
                        <div class="row">
                            @foreach($data as $image)
                                <div class="col-sm-3">
                                    <a href="#x" class="thumbnail"><img src="{{$image}}" alt="{{basename($image)}}"
                                                                        class="img-responsive"></a>
                                </div>
                            @endforeach
                        </div>
                        <!--/row-->
                    </div>
                    <!--/item-->
                @endforeach
            </div>
            <!--/carousel-inner-->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span
                        class="glyphicon glyphicon-chevron-left"></span></a>

            <a class="right carousel-control" href="#myCarousel" data-slide="next"><span
                        class="glyphicon glyphicon-chevron-right"></span></a>
        </div>
        <!--/myCarousel-->
    </div>
    <!--/well-->

    <div id="memeCanvas">
        <div class="row">
            <div class="col-md-8">
                <img class="img-responsive" src="{{'memes/cat2.jpg'}}" alt="" rel="memes/cat2.jpg">
            </div>
            <!-- end of div -->
            <div class="col-md-4">
                {!! Form::open(['url' => 'generatememe', 'method' => 'POST'] ) !!}

                <div class="form-group">

                    {!! Form::label('toptext', 'Texto Superior:') !!}

                    {!! Form::text('toptext', null, ["class" => "form-control"]) !!}

                </div>
                <!-- end of div -->

                <div class="form-group">

                    {!! Form::label('bottomtext', 'Texto Inferior:') !!}

                    {!! Form::text('bottomtext', null, ["class" => "form-control"]) !!}

                </div>
                <!-- end of div -->

                @if(Auth::check())

                    <div class="form-group">

                        {!! Form::submit('Crear meme!', ["class" => "btn btn-primary form-control"]) !!}

                    </div>
                    <!-- end of div -->
                @else
                    <div class="form-group">

                        <a data-toggle="modal" data-target="#loginModal" href="#">
                            <button class="btn btn-primary disabled form-control">Crear Meme</button>
                        </a>

                    </div>
                    <!-- end of div -->

                    <div class="alert alert-danger">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        <ul>
                            <p class="strong">Necesitas estar registrado/logeado para poder crear un meme.</p>
                        </ul>
                    </div>

                @endif


                {!! Form::hidden('image', 'memes/cat2.jpg') !!}

                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error )
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif

            </div>
            <!-- end of column -->
        </div>
        <!-- end of div -->
    </div>
    <!-- end of div with id memeCanvas -->
@stop

@section('sidebar')
    <p>sidebar content</p>
@stop

@section('footer')
    <script src="{{ asset('js/meme.js') }}"></script>
@stop