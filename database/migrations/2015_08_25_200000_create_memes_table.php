<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('meme_id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('author');
            $table->string('title');
            $table->string('top_text');
            $table->string('bottom_text');
            $table->string('meme_url');
            $table->timestamps();
        });

        Schema::table('memes', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('memes');
    }
}
