var elixir = require('laravel-elixir');
var BrowserSync = require('laravel-elixir-browsersync2');

elixir(function(mix) {
    mix.copy('node_modules/bootstrap-sass/assets/fonts/', 'public/fonts');
    mix.copy('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js', 'public/js');
    mix.copy('vendor/bower_dl/jquery/dist/jquery.min.js', 'public/js'); 
    mix.sass('app.scss') 
        .browserSync({proxy : 'memes.dev'});
});
