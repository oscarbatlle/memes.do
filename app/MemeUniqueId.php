<?php
namespace App;

/**
 * @author Oscar Batlle <oscarbatlle@gmail.com>
 */
class MemeUniqueId {

    /**
     * @param $salt
     * @return string
     */
    public function generateId($salt)
    {
        return substr(md5($salt . uniqid("", true)), 0, 6);
    }

}