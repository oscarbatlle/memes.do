<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model {

    protected $fillable = [
        'like',
        'unlike',
        'meme_id',
        'user_id'
    ];

    /**
     * Get the user who like/unlike the post
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function meme(){
        return $this->belongsTo('App\Meme', 'meme_id');
    }
}
