<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meme extends Model
{
    protected $fillable = [
        'title',
        'author',
        'top_text',
        'bottom_text',
        'meme_url',
        'user_id',
        'meme_id',
        'created_at',
    ];


    /**
     * Get the author
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
     * Get votes
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vote(){
        return $this->hasMany('App\Vote', 'meme_id');
    }

    public function getLikes($meme_id){
        return Vote::where('meme_id', $meme_id)->where('likes', '>', 0)->count();
    }

    public function getUnlikes($meme_id){
        return Vote::where('meme_id', $meme_id)->where('unlikes', '>', 0)->count();
    }
}
