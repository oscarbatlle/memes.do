<?php
/**
 * @author Oscar Batlle <oscarbatlle@gmail.com>
 */

namespace App;

use App\Repositories\UserRepository;
use Illuminate\Contracts\Auth\Guard as Authenticator;
Use Laravel\Socialite\Contracts\Factory as Socialite;
use Request;
class AuthenticateUser {

    /**
     * @var UserRepository
     */
    private $users;

    /**
     * @var Socialite
     */
    private $socialite;

    /**
     * @var Authenticator
     */
    private $auth;

    /**
     * @param UserRepository $users
     * @param Socialite $socialite
     * @param Authenticator $auth
     */
    public function __construct(UserRepository $users, Socialite $socialite, Authenticator $auth)
    {
        $this->users = $users;
        $this->socialite = $socialite;
        $this->auth = $auth;
    }

    /**
     * @param $request
     * @param $listener
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function execute($request, $listener, $provider)
    {
        if (!$request) return $this->getAuthorizationFirst($provider);

        $user = $this->users->findByUsernameOrCreate($this->getFacebookUser($provider));

        $this->auth->login($user, true);

        return $listener->userHasLoggedIn($user);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function getAuthorizationFirst($provider)
    {
        return $this->socialite->driver($provider)->redirect();

    }

    private function getFacebookUser($provider)
    {
        return $this->socialite->driver($provider)->user();
    }
}