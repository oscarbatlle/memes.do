<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'MemesController@index');
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Socialite
Route::get('login/{provider?}', 'Auth\AuthController@login');

// Meme Generator
Route::get('create', 'MemesController@create');
Route::post('generatememe', 'MemesController@store');

// Votes
Route::get('votes/{meme_id}', 'MemesController@getVotes');
// Like - Unlike
Route::post('votes/like/{meme_id}', 'MemesController@likePost');
Route::post('votes/unlike/{meme_id}', 'MemesController@unlikePost');

// View single post
Route::get('/meme/{id}', 'MemesController@show');
