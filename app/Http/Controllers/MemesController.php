<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMemeRequest;
use App\Meme;
use App\MemeGenerator;
use App\MemeUniqueId;
use App\Vote;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;

class MemesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $memes = Meme::orderBy('id', 'DESC')->paginate(5);

        return view('pages.index', compact('memes', 'votes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $images = glob("memes/*.{png,jpg,jpeg}", GLOB_BRACE);
        $memes = Meme::all();

        return view('pages.create', compact('memes', 'images'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|CreateMemeRequest $request
     * @return Response
     * @internal param MemeGenerator $meme
     */
    public function store(CreateMemeRequest $request) {
        $meme = new MemeGenerator($request->get('image'));
        $meme->setUpperText($request->get('toptext'));
        $meme->setLowerText($request->get('bottomtext'));
        if (Auth::check()) {
            $user = Auth::user()->name;
            $user_id = Auth::id();
        } else {
            $user = "Desconocido";
        }
        $generateid = new MemeUniqueId();
        $toptext = $request->get('toptext');
        $bottomtext = $request->get('bottomtext');
        $meme_id = $generateid->generateId($toptext);
        $memepath = "memes/created/" . $meme_id . ".jpg";
        $meme_url = 'meme/' . basename($memepath, '.jpg');
        $meme->processImg(0, $memepath);

        Meme::create(['title' => $toptext . " " . $bottomtext, 'user_id' => $user_id, 'author' => $user, 'meme_id' => $meme_id, 'meme_url' => $meme_url, 'top_text' => $toptext, 'bottom_text' => $bottomtext]);

        // Redirect to the created meme
        return redirect($meme_url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        // GET THE INFO OF THE MEME
        $meme = Meme::where('meme_id', $id)->first();

        // DISPLAY SINGLE MEME
        return view('pages.single', compact('meme'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    /**
     * Like post
     * @param Request $request
     * @param $meme_id
     * @internal param $user_id
     * @internal param $id
     */
    public function likePost(Request $request, $meme_id) {
        if ($request->user()) {
            $like = new Vote();
            $like->meme_id = $meme_id;
            $like->user_id = $request->user()->id;
            $like->likes += 1;
            $like->save();
        } else {
            $like = new Vote();
            $like->meme_id = $meme_id;
            $like->user_id = null;
            $like->likes += 1;
            $like->save();
        }
    }

    /**
     * unlike post
     * @param Request $request
     * @param $meme_id
     * @internal param $user_id
     * @internal param $id
     */
    public function unlikePost(Request $request, $meme_id) {
        if ($request->user()) {
            $unlike = new Vote();
            $unlike->meme_id = $meme_id;
            $unlike->user_id = $request->user()->id;
            $unlike->unlikes += 1;
            $unlike->save();
        } else {
            $unlike = new Vote();
            $unlike->meme_id = $meme_id;
            $unlike->user_id = null;
            $unlike->unlikes += 1;
            $unlike->save();
        }
    }
}
