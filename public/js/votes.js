$('.like').one('click', function (e) {
    e.preventDefault();
    // Disable button
    $(this).addClass('disabled');
    $(this).click(false);
    // Get the id of the current meme
    var meme_id = $(this).closest('.votes').attr('data-entry-id');
    // Find the number of votes
    var likes = parseInt($(this).closest('.votes').find('.like-count').text());
    // Sum 1 to the total votes
    var countUp = likes + 1;
    // Update count
    $(this).closest('.votes').find('.like-count').text(countUp);
    // Save in DB
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });
    $.post('http://memes.dev/votes/like/' + meme_id);
});

$('.unlike').one('click', function (e) {
    e.preventDefault();
    // Disable button
    $(this).addClass('disabled');
    $(this).click(false);
    // Get the id of the current meme
    var meme_id = $(this).closest('.votes').attr('data-entry-id');
    // Find the number of votes
    var unlikes = parseInt($(this).closest('.votes').find('.unlike-count').text());
    // Take off 1 from the total votes
    var countDown = unlikes + 1;
    // Update count
    $(this).closest('.votes').find('.unlike-count').text(countDown);
    // Save in DB
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });
    $.post('http://memes.dev/votes/unlike/' + meme_id);
});